#!/usr/bin/env bash -x


LEAVES="$HOME/mac_os-config/brew-leaves.txt"
if [[ -e "$LEAVES" ]]; then
	cat $LEAVES | xargs -n1 -I % brew install %
fi
