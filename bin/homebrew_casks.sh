#!/usr/bin/env bash -x

CASKS="$HOME/mac_os-config/casks.txt"
if [[ -e "$CASKS" ]]; then
        cat $CASKS | xargs -n1 -p -I % brew cask install  %
fi

